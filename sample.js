let http = require("http");

http.createServer(function(request,response){

 if(request.url === "/"){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello from our sample Server");
}else if (request.url ==="/profile"){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hi! I'm Josue!");
}
}).listen(5000);

console.log("Server is running on localhost:5000!");
